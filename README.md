## Commands
- app:updateExchangeRate

update currency prices peer based on a third part API named `NAVASAN`
a schedule set for this command for every minutes so run this command as this CLI command for running without cronjob :

`php artisan schedule:run-cronless`

## APIs
- /api/order/inquiry [POST]

for inquiry and check result based on our inputs and cached it for 5 seconds

`REQUEST BODY` :

        {
            "from":"",
            "to":"",
            "amount": "",
            "email":""
        }

- /api/order/store [POST]

    `REQUEST BODY` :

        {
            "email":""
        }
store cached order in `inquiry step` in order table and get track ID

- /api/order/trackID [GET]

get detail of order
