<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/register', 'App\Http\Controllers\AuthController@register');
Route::post('/login', 'App\Http\Controllers\AuthController@login');
    Route::prefix('/order')->group(function(){
        Route::post('/inquiry', 'App\Http\Controllers\OrderController@inquiry');
        Route::post('/store', 'App\Http\Controllers\OrderController@store');
        Route::get('/{id}', 'App\Http\Controllers\OrderController@show');
    });

