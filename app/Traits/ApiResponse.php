<?php
namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

trait ApiResponse
{
    /**
     * Default status code
     * @var int $statusCode
     */
    private $statusCode = Response::HTTP_OK;

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     * @return ApiResponse
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @param string $message
     * @return JsonResponse
     */
    public function badRequestResponse($message = 'Bad request!')
    {
        return $this->setStatusCode(Response::HTTP_BAD_REQUEST)->errorResponse($message);
    }

    /**
     * @param string $message
     * @return JsonResponse
     */
    public function notFoundResponse($message = 'Not found!')
    {
        return $this->setStatusCode(Response::HTTP_NOT_FOUND)->errorResponse($message);
    }

    /**
     * @param string $message
     * @return JsonResponse
     */
    public function forbiddenResponse($message = 'Forbidden, access denied!')
    {
        return $this->setStatusCode(Response::HTTP_FORBIDDEN)->errorResponse($message);
    }

    /**
     * @param string $message
     * @return JsonResponse
     */
    public function unAuthorizedResponse($message = 'Request is unauthorized!')
    {
        return $this->setStatusCode(Response::HTTP_UNAUTHORIZED)->errorResponse($message);
    }

    /**
     * @param $message
     * @return JsonResponse
     */
    public function errorResponse($message)
    {
        return $this->respond([
            'status' => $this->getStatusCode(),
            'errors' => $message
        ]);
    }

    /**
     * @param $message
     * @return JsonResponse
     */
    public function successResponse($message)
    {
        return $this->respond([
            'status' => $this->getStatusCode(),
            'result' => $message
        ]);
    }

    /**
     * @param $data
     * @param array $headers
     * @return JsonResponse
     */
    public function respond($data/*, $headers = []*/)
    {
        return new JsonResponse($data, $this->getStatusCode());
    }


}
