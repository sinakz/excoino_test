<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];
    use HasFactory;
    public static function boot()
    {
        parent::boot();
        static::creating(function ($order) {
            $key = $order->user->email . now()->timestamp;
            $order->track_id = md5($key);
        });

    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
