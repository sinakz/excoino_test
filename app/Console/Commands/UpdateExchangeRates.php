<?php

namespace App\Console\Commands;

use App\Models\ExchangeRate;
use App\Services\Price\PriceInterface;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use function App\convertTomanToRial;

class UpdateExchangeRates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:updateExchangeRate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update prices based on third-party APIs';
    /**
     * @var PriceInterface
     */
    private $priceInterface;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(PriceInterface $priceInterface)
    {
        parent::__construct();
        $this->priceInterface = $priceInterface;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $result = $this->priceInterface->getRealtimePrices();
        foreach ($result as $symbol=>$value){
            ExchangeRate::updateOrCreate(
                ['symbol' => $symbol],
                ['symbol' => $symbol, 'rate' => $value]
            );
        }
    }
}
