<?php

namespace App\Providers;

use App\Services\Price\NavasanService;
use App\Services\Price\PriceInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(PriceInterface::class, NavasanService::class);
    }
}
