<?php


namespace App\Repositories;


abstract class Repository implements RepositoryInterface
{
    protected $model;
    abstract public function model();
    public function __construct()
    {
        $this->model = app()->make($this->model());
    }

    public function all()
    {
        return $this->model::all();
    }
}
