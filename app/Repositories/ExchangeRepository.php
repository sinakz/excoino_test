<?php


namespace App\Repositories;


class ExchangeRepository extends Repository
{
    public function model()
    {
        return 'App\Models\ExchangeRate';
    }
    public function getBasedToPeerRate($peerCurrency)
    {
        return $this->model->where("symbol", env('BASE_CURRENCY_SYMBOL','IRT').$peerCurrency)->firstOrFail();
    }
}
