<?php


namespace App\Services\Inquiry;


use App\Repositories\ExchangeRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class InDirectInquiryStrategy implements InquiryInterface
{
    public $inquiryService;
    private $payload;
    private $exchangeRateRepository;

    public function __construct($payload)
    {
        $this->inquiryService = new InquiryService($payload);
        $this->payload = $payload;
        $this->exchangeRateRepository = new ExchangeRepository();
    }
    public function inquiry()
    {
        try {
            $firstSideRate = $this->exchangeRateRepository->getBasedToPeerRate($this->payload['from']);
            $secondSideRate = $this->exchangeRateRepository->getBasedToPeerRate($this->payload['to']);
        }catch (ModelNotFoundException $exception){
            return "exchange peer not found !";
        }
        $convertToBaseCurrency = $this->payload['amount'] * $firstSideRate['rate'];
        return [
            'paid_amount'=> $this->payload['amount'],
            'receive_amount'=> $convertToBaseCurrency / $secondSideRate['rate'],
            'rate'=>$secondSideRate->rate,
            'symbol'=>$this->payload['from'].$this->payload['to'],
        ];
    }
}
