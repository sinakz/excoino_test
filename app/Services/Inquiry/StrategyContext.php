<?php


namespace App\Services\Inquiry;


class StrategyContext
{
    public $inquiryService;
    private $strategy = null;
    public function __construct($payload)
    {
        $this->inquiryService = new InquiryService($payload);
        if ($this->inquiryService->isBaseEngaged()){
            $this->strategy = new DirectInquiryStrategy($payload);
        }else{
            $this->strategy = new InDirectInquiryStrategy($payload);
        }
    }
    public function getStrategy()
    {
        return $this->strategy;
    }
}
