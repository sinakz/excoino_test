<?php


namespace App\Services\Inquiry;


class InquiryService
{
    public function __construct(public $payload)
    {}
    public function isBaseEngaged()
    {
        return in_array(env('BASE_CURRENCY_SYMBOL','IRT'),[$this->payload['from'], $this->payload['to']]);
    }
    public function getNonBaseSymbol()
    {
        return implode(array_diff( [$this->payload['from'], $this->payload['to']], [env('BASE_CURRENCY_SYMBOL','IRT')] ));
    }
    public function calcReceiveAmount($requestedSymbol, $existedSymbol, $rate, $amount)
    {
        if ($this->isDoubleExchangeExists($requestedSymbol, $existedSymbol)){
            return round($this->payload['amount'] / $rate, 3);
        }
        return $this->payload['amount'] * $rate;
    }
    private function isDoubleExchangeExists($requestedSymbol, $existedSymbol)
    {
        return $requestedSymbol === $existedSymbol;
    }

}
