<?php


namespace App\Services\Inquiry;

use App\Repositories\ExchangeRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class DirectInquiryStrategy implements InquiryInterface
{
    public $payload;
    public $inquiryService;
    public $exchangeRateRepository;

    public function __construct($payload)
    {
        $this->payload = $payload;
        $this->inquiryService = new InquiryService($payload);
        $this->exchangeRateRepository = new ExchangeRepository();
    }

    public function inquiry()
    {
        $requestedSymbol = $this->payload['from'] . $this->payload['to'];
        try {
            $rate = $this->exchangeRateRepository->getBasedToPeerRate($this->inquiryService->getNonBaseSymbol());
        } catch (ModelNotFoundException $exception) {
            return "exchange peer not found !";
        }
        $receiveAmount = $this->inquiryService->calcReceiveAmount($requestedSymbol, $rate->symbol, $rate->rate,$this->payload['amount'] );
        return [
            'paid_amount' => $this->payload['amount'],
            'receive_amount' => $receiveAmount,
            'rate' => $rate->rate,
            'symbol' => $requestedSymbol,
        ];
    }
}
