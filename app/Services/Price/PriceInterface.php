<?php


namespace App\Services\Price;


Interface PriceInterface
{
    public function getRealtimePrices();
}
