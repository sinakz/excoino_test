<?php


namespace App\Services\Price;


use GuzzleHttp\Client;
use function App\convertTomanToRial;

class NavasanService implements PriceInterface
{
    public function getRealtimePrices()
    {
        $client = new Client();
        $response = $client->get( env("PRICE_CHANGES_API_URL"));
        $responseResult = json_decode($response->getBody(),1);
        return [
            'IRTUSD'=>convertTomanToRial($responseResult['usd_buy']['value']),
            'IRTEU'=>convertTomanToRial($responseResult['eur_pp']['value']),
        ];

    }
}
