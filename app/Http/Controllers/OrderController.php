<?php

namespace App\Http\Controllers;

use App\Http\Requests\InquiryRequest;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Resources\ShowOrderResource;
use App\Models\ExchangeRate;
use App\Models\Order;
use App\Models\User;
use App\Services\Inquiry\StrategyContext;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class OrderController extends ApiController
{
    /**
     * @param InquiryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function inquiry(InquiryRequest $request)
    {

        try {
            $strategy = new StrategyContext($request->all(['from', 'to', 'amount']));
        }catch (\Exception $exception){
            $this->forbiddenResponse($exception->getMessage());
        }
        $inquiry = $strategy->getStrategy()->inquiry();
        Cache::put($request->get('email').'_inquiry_order', $inquiry, 5000);
        return $this->successResponse($inquiry);
    }

    /**
     * @param StoreOrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreOrderRequest $request)
    {
        $cacheKey = $request->get('email').'_inquiry_order';
        $cachedInquiry = Cache::get($cacheKey);
        if (!$cachedInquiry){
            return $this->badRequestResponse("no cached inquiry found!");
        }
        $user = User::firstOrCreate([
            'email' => $request->get('email')
        ], [
            'email' => $request->get('email'),
        ]);
        $cachedInquiry['user_id'] = $user->id;
        $newOrder = Order::create($cachedInquiry);
        Cache::forget($cacheKey);
        return $this->successResponse($newOrder->track_id);
    }
    public function show($trackId)
    {
        try {
            $order = Order::where('track_id', $trackId)->firstOrFail();
        }catch (ModelNotFoundException $exception){
            return $this->notFoundResponse($exception->getMessage());
        }
        $showOrderResource = new ShowOrderResource($order);
        return $this->successResponse($showOrderResource);
    }
}
