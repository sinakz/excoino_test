<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShowOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'paid_amount'=> $this->paid_amount,
            'receive_amount'=> $this->receive_amount,
            'symbol'=> $this->symbol,
            'rate'=> $this->rate,
            'created_at'=> $this->created_at,
        ];
    }
}
